export default {
  primary: '#00C464',
  secondary: '#317688',
  base: '#fff',
  body: '#fdfdfd',
  text: '#333',
  textInverted: '#fff',
  borders: '#ddd',
  disabled: '#80bfa0',
  error: '#ff7777',
  hover: '#00E864',
};
