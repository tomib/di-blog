export const POST_EDIT = 'POST_EDIT';
export const POST_DELETE = 'POST_DELETE';

export const POSTS_CLEAR = 'POSTS_CLEAR';
export const POSTS_FETCHING = 'POSTS_FETCHING';
export const POSTS_GET = 'POSTS_GET';

export const PROFILE_GET = 'PROFILE_GET';
