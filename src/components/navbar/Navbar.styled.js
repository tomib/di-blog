import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import colors from '../../constants/colors';

export const NavbarStyled = styled.div`
  display: table;
  
  width: 100%;

  padding: 20px;
`;

export const Left = styled.div`
  display: table-cell;
  vertical-align: middle;

  width: 75px;
  min-width: 75px;

  text-align: left;
`;

export const Title = styled.div`
  font-size: 18px;
  font-weight: 700;
`;

export const Right = styled.div`
  display: table-cell;
  vertical-align: middle;

  width: 100%;

  text-align: right;
`;

export const Profile = styled.div`
  display: none;

  margin-right: 5px;
  padding-right: 15px;

  border-right: 1px solid ${colors.borders};

  @media (min-width: 640px) {
    display: inline-block;
    vertical-align: middle;
  }
`;

export const Link = styled(NavLink)`
  display: inline-block;
  vertical-align: middle;

  margin-left: 10px;
  padding: 5px;

  color: ${colors.text};

  text-decoration: none;

  transition: 0.3s all ease-in-out;

  &.active, &:hover {
    color: ${colors.primary};
  }
`;
