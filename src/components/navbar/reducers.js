import { PROFILE_GET } from '../../constants/actions';

const initialState = {
  name: '',
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case PROFILE_GET:
      return Object.assign({}, state, {
        name: payload,
      });
    default:
      return state;
  }
};
