import axios from 'axios';

import { PROFILE_GET } from '../../constants/actions';

export const getProfile = () => (dispatch) => {
  axios.get('http://localhost:3001/profile', {})
    .then((res) => {
      dispatch({
        type: PROFILE_GET,
        payload: res.data.name,
      });
    })
    .catch(() => {});
};

export default getProfile;
