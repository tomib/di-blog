import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { getProfile } from './actions';

import {
  NavbarStyled,
  Left,
  Title,
  Right,
  Profile,
  Link,
} from './Navbar.styled';

export class Navbar extends Component {
  componentDidMount() {
    this.props.getProfile();
  }

  render() {
    return (
      <NavbarStyled data-test="Navbar">
        <Left>
          <Title>DI-Blog</Title>
        </Left>
        <Right>
          {this.props.profile.name && (
            <Profile>Hello <strong>{this.props.profile.name}</strong>!</Profile>
          )}
          <Link exact to="/" activeClassName="active">Home</Link>
          <Link to="/create" activeClassName="active">Create New</Link>
        </Right>
      </NavbarStyled>
    );
  }
}

Navbar.propTypes = {
  getProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    getProfile,
  }, dispatch)
);

const mapStateToProps = ({ profile }) => ({
  profile,
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar));
