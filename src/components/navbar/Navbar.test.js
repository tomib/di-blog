import React from 'react';
import { shallow } from 'enzyme';
import { Navbar } from './Navbar';

describe('tests the Navbar component', () => {
  const wrapper = shallow(<Navbar getProfile={() => {}} profile={{ name: 'Test User' }} />);

  it('renders the Navbar component', () => {
    expect(wrapper.find('[data-test="Navbar"]').exists()).toBe(true);
  });
});
