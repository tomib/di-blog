import styled from 'styled-components';

import colors from '../../../constants/colors';

export const FormStyled = styled.form`
  display: block;
  
  width: 100%;
`;

export const Label = styled.label`
  display: block;
  position: relative;
  
  width: 100%;

  margin-bottom: 10px;
  padding-bottom: 20px;

  span {
    display: block;

    margin-bottom: 10px;

    font-size: 14px;

    @media (min-width: 480px) {
      font-size: 16px;
    }
  }
`;

export const Input = styled.input`
  display: block;
  
  width: 100%;

  padding: 10px;

  background: ${colors.base};

  font-size: 14px;

  border: 1px solid ${props => (props.error ? colors.error : colors.borders)};
  border-radius: 4px;
  outline: 0;

  @media (min-width: 480px) {
    padding: 10px 15px;

    font-size: 16px;
  }
`;

export const Textarea = styled.textarea`
  display: block;
  
  width: 100%;
  min-width: 100%;
  max-width: 100%;
  min-height: 320px;

  padding: 10px;

  background: ${colors.base};

  font-size: 14px;
  line-height: 1.3;

  border: 1px solid ${props => (props.error ? colors.error : colors.borders)};
  border-radius: 4px;
  outline: 0;

  @media (min-width: 480px) {
    padding: 15px;

    font-size: 16px;
  }
`;

export const Error = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;

  color: ${colors.error};

  font-size: 13px;
`;

export const Notification = styled.div`
  margin-bottom: 20px;
  padding: 10px 15px;

  background: ${props => (props.type === 'error' && colors.error)};
  color: ${props => (props.type === 'error' && colors.textInverted)};

  border-radius: 4px;

  font-size: 13px;
`;

export const Controls = styled.div`
  display: table;

  width: 100%;
`;

export const Left = styled.div`
  display: table-cell;
  vertical-align: middle;

  text-align: left;
`;

export const Right = styled.div`
  display: table-cell;
  vertical-align: middle;

  text-align: right;
`;
