import React from 'react';
import { shallow } from 'enzyme';
import Button from './Button';

describe('tests the Button component', () => {
  const wrapper = shallow(<Button />);

  it('renders the Button component', () => {
    expect(wrapper.find('[data-test="Button"]').exists()).toBe(true);
  });
});
