import styled from 'styled-components';

import colors from '../../../constants/colors';

export const ButtonStyled = styled.button`
  display: inline-block;

  min-width: 80px;

  padding: 10px 15px;

  background: ${props => (props.appearance === 'primary' && colors.primary)
  || (props.appearance === 'ghost' && 'transparent')};
  color: ${props => (props.appearance === 'primary' && colors.textInverted)
  || (props.appearance === 'ghost' && colors.primary)};

  font-size: 13px;
  font-weight: 700;

  border: 1px solid ${colors.primary};
  background: ${props => (props.appearance === 'primary' && colors.primary)
  || (props.appearance === 'ghost' && 'transparent')};
  border-radius: 4px;
  outline: none;

  box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.15);
  opacity: ${props => (props.disabled ? 0.3 : 1)};

  cursor: pointer;

  transition: 0.3s all ease-in-out;

  @media (min-width: 480px) {
    min-width: 100px;

    padding: 12px 25px;

    font-size: 16px;
  }

  &:hover:not(:disabled) {
    background: ${props => (props.appearance === 'primary' && colors.hover)};
    color: ${props => (props.appearance === 'ghost' && colors.hover)};

    border: 1px solid ${colors.hover};

    transform: ${props => (props.appearance === 'primary' && 'translateY(-2px)')};
  }
`;

export default ButtonStyled;
