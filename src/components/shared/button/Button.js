import React from 'react';
import PropTypes from 'prop-types';

import { ButtonStyled } from './Button.styled';

const Button = props => (
  <ButtonStyled
    appearance={props.appearance}
    data-test="Button"
    disabled={props.disabled}
    onClick={() => props.handleClick()}
    type={props.type}
  >
    {props.children}
  </ButtonStyled>
);

Button.propTypes = {
  appearance: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  disabled: PropTypes.bool,
  handleClick: PropTypes.func,
  type: PropTypes.string,
};

Button.defaultProps = {
  appearance: 'primary',
  children: 'Button',
  disabled: false,
  handleClick: () => {},
  type: 'button',
};

export default Button;
