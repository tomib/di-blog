import React from 'react';
import { shallow } from 'enzyme';
import { Edit } from './Edit';

describe('tests the Edit component', () => {
  const wrapper = shallow(<Edit clearPosts={() => {}} history={{}} match={{ params: { id: 0 } }} />);
  wrapper.setState({ loaded: true });

  it('renders the Edit component', () => {
    expect(wrapper.find('[data-test="Edit"]').exists()).toBe(true);
  });
});
