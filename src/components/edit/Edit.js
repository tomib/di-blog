import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import PropTypes from 'prop-types';

import { EditStyled, Title } from './Edit.styled';
import Form from './form/Form';

import { clearPosts } from '../posts/actions';

export class Edit extends Component {
  state = {
    loaded: false,
    body: '',
    title: '',
  }

  componentDidMount() {
    this.getPostData(this.props.match.params.id);
  }

  onCancel = () => {
    this.props.history.push('/');
  }

  onPostEdited = () => {
    this.props.clearPosts();
    this.props.history.push('/');
  }

  getPostData = (id) => {
    axios.get(`http://localhost:3001/posts/${id}`, {})
      .then((res) => {
        this.setState(() => ({
          loaded: true,
          body: res.data.body,
          title: res.data.title,
        }));
      })
      .catch(() => {
        this.props.history.push('/');
      });
  }

  render() {
    return this.state.loaded && (
      <EditStyled data-test="Edit">
        <Title>Edit the post</Title>
        <Form
          body={this.state.body}
          handleCancel={this.onCancel}
          handleSuccess={this.onPostEdited}
          id={Number(this.props.match.params.id)}
          title={this.state.title}
        />
      </EditStyled>
    );
  }
}

Edit.propTypes = {
  clearPosts: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    clearPosts,
  }, dispatch)
);

export default withRouter(connect(null, mapDispatchToProps)(Edit));
