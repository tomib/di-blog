import React, { Component } from 'react';
import axios from 'axios';
import { Formik } from 'formik';
import { object, string } from 'yup';
import PropTypes from 'prop-types';

import {
  FormStyled,
  Label,
  Input,
  Textarea,
  Error,
  Notification,
  Controls,
  Left,
  Right,
} from '../../shared/form/Form.styled';
import Button from '../../shared/button/Button';

class Form extends Component {
  render() {
    return (
      <Formik
        initialValues={{
          title: this.props.title,
          body: this.props.body,
        }}
        validationSchema={object().shape({
          title: string().trim()
            .required('Please enter the title of your post.'),
          body: string().trim()
            .required('Please enter the body of your post.'),
        })}
        onSubmit={(
          { title, body },
          { setSubmitting, setStatus },
        ) => {
          setSubmitting(true);
          setStatus({
            serverError: false,
          });

          axios.put(`http://localhost:3001/posts/${this.props.id}`, {
            title,
            body,
          })
            .then(() => {
              setSubmitting(false);
              this.props.handleSuccess();
            })
            .catch(() => {
              setSubmitting(false);
              setStatus({
                serverError: true,
              });
            });
        }}
        render={({
          values,
          errors,
          status,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <FormStyled onSubmit={handleSubmit}>
            {status && status.serverError && (
              <Notification type="error">There has been an error communicating with the server. Please try again later.</Notification>
            )}
            <Label>
              <span>Post title:</span>
              <Input
                type="text"
                name="title"
                error={touched.title && errors.title}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.title}
              />
              {touched.title && errors.title && <Error>{errors.title}</Error>}
            </Label>
            <Label>
              <span>Post body:</span>
              <Textarea
                type="text"
                name="body"
                error={touched.body && errors.body}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.body}
              />
              {touched.body && errors.body && <Error>{errors.body}</Error>}
            </Label>
            <Controls>
              <Left>
                <Button
                  appearance="ghost"
                  handleClick={this.props.handleCancel}
                  type="button"
                >
                  Cancel
                </Button>
              </Left>
              <Right>
                <Button type="submit" disabled={isSubmitting}>
                  Submit
                </Button>
              </Right>
            </Controls>
          </FormStyled>
        )}
      />
    );
  }
}

Form.propTypes = {
  body: PropTypes.string.isRequired,
  handleCancel: PropTypes.func.isRequired,
  handleSuccess: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};

export default Form;
