import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { CreateStyled, Title } from './Create.styled';
import Form from './form/Form';

import { clearPosts } from '../posts/actions';

export class Create extends Component {
  onCancel = () => {
    this.props.history.push('/');
  }

  onPostCreated = () => {
    this.props.clearPosts();
    this.props.history.push('/');
  }

  render() {
    return (
      <CreateStyled data-test="Create">
        <Title>Create a new post</Title>
        <Form
          handleCancel={this.onCancel}
          handleSuccess={this.onPostCreated}
        />
      </CreateStyled>
    );
  }
}

Create.propTypes = {
  clearPosts: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    clearPosts,
  }, dispatch)
);

export default withRouter(connect(null, mapDispatchToProps)(Create));
