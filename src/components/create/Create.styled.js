import styled from 'styled-components';

import colors from '../../constants/colors';

export const CreateStyled = styled.div`
  display: block;
  
  width: 100%;
  max-width: 760px;

  margin: 0 auto;

  padding: 40px 30px;

  background: ${colors.base};

  border-radius: 4px;

  box-shadow: 0px 4px 15px 0px rgba(0,0,0,0.1);

  @media (min-width: 480px) {
    padding: 40px 60px;
  }
`;

export const Title = styled.h1`
  margin-bottom: 40px;

  font-size: 24px;
  text-align: center;

  @media (min-width: 480px) {
    margin-bottom: 40px;

    font-size: 30px;
  }
`;
