import React from 'react';
import { shallow } from 'enzyme';
import { Create } from './Create';

describe('tests the Create component', () => {
  const wrapper = shallow(<Create clearPosts={() => {}} history={{}} />);

  it('renders the Create component', () => {
    expect(wrapper.find('[data-test="Create"]').exists()).toBe(true);
  });
});
