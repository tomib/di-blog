import styled from 'styled-components';

import colors from '../../constants/colors';

export const PostsStyled = styled.div`
  width: 100%;
  max-width: 960px;

  margin: 0 auto;
`;

export const Empty = styled.div`
  padding: 120px 40px;

  background: ${colors.base};

  text-align: center; 

  border-radius: 4px;

  box-shadow: 0px 4px 15px 0px rgba(0,0,0,0.1);
`;

export const Controls = styled.div`
  display: block;

  width: 100%;

  text-align: center;
`;
