import axios from 'axios';

import {
  POSTS_CLEAR,
  POSTS_FETCHING,
  POSTS_GET,
  POST_DELETE,
} from '../../constants/actions';

export const getPosts = page => (dispatch) => {
  dispatch({
    type: POSTS_FETCHING,
    payload: true,
  });

  axios.get('http://localhost:3001/posts', {
    params: {
      _limit: 10,
      _order: 'desc',
      _page: page,
      _sort: 'id',
    },
  })
    .then((res) => {
      dispatch({
        type: POSTS_GET,
        payload: {
          data: res.data,
          page,
          totalPosts: res.headers['x-total-count'] || 0,
        },
      });

      dispatch({
        type: POSTS_FETCHING,
        payload: false,
      });
    })
    .catch(() => {
      dispatch({
        type: POSTS_FETCHING,
        payload: false,
      });
    });
};

export const deletePost = id => (dispatch) => {
  axios.delete(`http://localhost:3001/posts/${id}`, {})
    .then(() => {
      dispatch({
        type: POST_DELETE,
      });

      dispatch(getPosts(1));
    })
    .catch(() => {});
};

export const clearPosts = () => (dispatch) => {
  dispatch({
    type: POSTS_CLEAR,
  });
};
