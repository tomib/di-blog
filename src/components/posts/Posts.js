import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { PostsStyled, Empty, Controls } from './Posts.styled';
import Post from './post/Post';
import Button from '../shared/button/Button';

import { deletePost, getPosts } from './actions';

export class Posts extends Component {
  componentDidMount() {
    if (this.props.posts.data.length === 0) {
      this.props.getPosts(1);
    }

    window.addEventListener('scroll', this.onScroll, false);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false);
  }

  onGetMorePosts = () => {
    this.props.getPosts(this.props.posts.page + 1);
  }

  onEditPost = (id) => {
    this.props.history.push(`/edit/${id}`);
  }

  onDeletePost = (id) => {
    this.props.deletePost(id);
  }

  onScroll = () => {
    const {
      fetching,
      data,
      totalPosts,
      page,
    } = this.props.posts;
    /*
      There are a couple of conditions here - posts currently not being fetched, initial data finished loading and the total number of posts is larger than the user has fetched so far. If all those conditions pass, the height of the window and the scroll amount on the Y Axis are being added and compared with the size of the body to detect if the user has scrolled to the bottom of the page.
    */
    if (!fetching && data.length > 0 && (totalPosts > page * 10)) {
      if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        this.onGetMorePosts();
      }
    }
  }

  render() {
    return (
      <PostsStyled data-test="Posts">
        {this.props.posts.data.length === 0 && (
          <Empty>No posts have been found.</Empty>
        )}
        {this.props.posts.data.map((item, index) => (
          <Post
            data={item}
            key={index.toString()}
            onEditPost={() => this.onEditPost(item.id)}
            onDeletePost={() => this.onDeletePost(item.id)}
          />
        ))}
        {this.props.posts.totalPosts > this.props.posts.page * 10 && (
          <Controls>
            <Button
              disabled={this.props.posts.fetching}
              handleClick={this.onGetMorePosts}
              type="button"
            >
              Load More Posts
            </Button>
          </Controls>
        )}
      </PostsStyled>
    );
  }
}

Posts.propTypes = {
  deletePost: PropTypes.func.isRequired,
  getPosts: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  posts: PropTypes.object,
};

Posts.defaultProps = {
  posts: {
    data: [],
    fetching: false,
    page: 1,
    totalPosts: 0,
  },
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    deletePost,
    getPosts,
  }, dispatch)
);

const mapStateToProps = ({ posts }) => ({
  posts,
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
