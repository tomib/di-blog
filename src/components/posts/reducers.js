import {
  POSTS_CLEAR,
  POSTS_FETCHING,
  POSTS_GET,
  POST_DELETE,
} from '../../constants/actions';

const initialState = {
  data: [],
  fetching: false,
  page: 1,
  totalPosts: 0,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case POSTS_CLEAR:
      return initialState;
    case POSTS_GET:
      return Object.assign({}, state, {
        data: state.data.concat(payload.data),
        page: payload.page,
        totalPosts: payload.totalPosts,
      });
    case POST_DELETE:
      return Object.assign({}, state, {
        data: [],
        page: 1,
        totalPosts: 0,
      });
    case POSTS_FETCHING:
      return Object.assign({}, state, {
        fetching: payload,
      });
    default:
      return state;
  }
};
