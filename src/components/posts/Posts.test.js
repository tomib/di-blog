import React from 'react';
import { shallow } from 'enzyme';
import { Posts } from './Posts';

describe('tests the Posts component', () => {
  const wrapper = shallow(<Posts deletePost={() => {}} getPosts={() => {}} />);

  it('renders the Posts component', () => {
    expect(wrapper.find('[data-test="Posts"]').exists()).toBe(true);
  });
});
