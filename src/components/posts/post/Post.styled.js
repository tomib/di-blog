import styled from 'styled-components';

import colors from '../../../constants/colors';

export const PostStyled = styled.div`
  display: block;
  
  width: 100%;

  margin-bottom: 30px;
  padding: 20px;

  background: ${colors.base};

  border-radius: 4px;

  box-shadow: 0px 4px 15px 0px rgba(0,0,0,0.1);

  @media (min-width: 480px) {
    padding: 20px 40px;
  }
`;

export const Title = styled.h1`
  margin-bottom: 20px;

  font-size: 24px;

  @media (min-width: 480px) {
    font-size: 30px;
  }
`;

export const Body = styled.div`
  margin-bottom: 20px;
  padding-bottom: 30px;

  font-size: 13px;
  line-height: 1.5;
  white-space: pre-wrap;

  border-bottom: 1px solid ${colors.borders};

  @media (min-width: 480px) {
    font-size: 16px;
  }
`;

export const Controls = styled.div`
  display: block;
  
  width: 100%;

  text-align: right;

  button {
    margin-left: 10px;

    @media (min-width: 480px) {
      margin-left: 15px;
    }
  }
`;
