import React from 'react';
import { shallow } from 'enzyme';
import Post from './Post';

describe('tests the Post component', () => {
  const wrapper = shallow(<Post onEditPost={() => {}} onDeletePost={() => {}} />);

  it('renders the Post component', () => {
    expect(wrapper.find('[data-test="Post"]').exists()).toBe(true);
  });
});
