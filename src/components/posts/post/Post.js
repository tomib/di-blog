import React from 'react';
import PropTypes from 'prop-types';

import {
  PostStyled,
  Title,
  Body,
  Controls,
} from './Post.styled';
import Button from '../../shared/button/Button';

const Post = props => (
  <PostStyled data-test="Post">
    <Title>{props.data.title}</Title>
    <Body>{props.data.body}</Body>
    <Controls>
      <Button
        handleClick={props.onEditPost}
      >
        Edit Post
      </Button>
      <Button
        handleClick={props.onDeletePost}
      >
        Delete Post
      </Button>
    </Controls>
  </PostStyled>
);

Post.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    body: PropTypes.string,
  }),
  onEditPost: PropTypes.func.isRequired,
  onDeletePost: PropTypes.func.isRequired,
};

Post.defaultProps = {
  data: {
    title: '',
    body: '',
  },
};

export default Post;
