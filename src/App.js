import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Navbar from './components/navbar/Navbar';
import Posts from './components/posts/Posts';
import Create from './components/create/Create';
import Edit from './components/edit/Edit';

import { AppStyled, Page } from './App.styled';

const App = () => (
  <AppStyled data-test="App">
    <Navbar />
    <Page>
      <Switch>
        <Route exact path="/" component={Posts} />
        <Route path="/create" component={Create} />
        <Route path="/edit/:id" component={Edit} />
        <Redirect to="/" />
      </Switch>
    </Page>
  </AppStyled>
);

export default App;
