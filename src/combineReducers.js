import { combineReducers } from 'redux';

import profileReducer from './components/navbar/reducers';
import postsReducer from './components/posts/reducers';

export default combineReducers({
  profile: profileReducer,
  posts: postsReducer,
});
