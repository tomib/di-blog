import styled from 'styled-components';

export const AppStyled = styled.div`
  width: 100%;
  max-width: 960px;

  margin: 0 auto;
`;

export const Page = styled.div`
  display: block;

  width: 100%;

  padding: 10px 10px 20px 10px;

  @media (min-width: 480px) {
    padding: 20px 20px 40px 20px;
  }
`;
