# DI-Blog

A React blogging application with infinite scroll that supports creating, editing and deleting posts. A dummy backend has been implemented using JSON Server. There are some basic render tests added, but nothing indepth for this particular task.

Webpack template based on: [React Init](https://github.com/tomislavbisof/react-init-sc)

## Includes

* [Styled Components](https://www.styled-components.com/)
* [ESLint](https://eslint.org)
* [Airbnb Style Guide](https://github.com/airbnb/javascript)
* [Webpack 4](https://webpack.js.org)
* [React Router](https://reacttraining.com/react-router)
* [Redux](https://redux.js.org/)
* [Redux Thunk](https://github.com/gaearon/redux-thunk)
* [Formik](https://github.com/jaredpalmer/formik)
* [Yup](https://github.com/jquense/yup)
* [Axios](https://github.com/axios/axios)
* [PropTypes](https://github.com/facebook/prop-types)
* [Enzyme](http://airbnb.io/enzyme/)
* [Jest](https://facebook.github.io/jest/)

### Installation

To install dependencies, clone the repository, navigate to the root folder of it and run:

```
npm install
```

After that, you have to install the JSON Server using:

```
npm install -g json-server
```

### Usage

In the project root folder, start the JSON Server using:

```
json-server --watch db.json --port 3001
```

For development with the Webpack Dev Server and HMR, use:

```
npm run watch
```

To build the project for production, use:

```
npm run build
```

Your project files will be located in the `/dist/` folder after building. Images smaller than 8KB will be embedded, while larger ones will be located in `/dist/assets/`.

To run tests, use:

```
npm run test
```
